"""
    Autor: Bruno Gomez
"""

# Mejoras pendientes:
# 1. incorporar seccion de chequeos globales - acutalmente solo funciona para chequeos de query
# 2. mejorar el diseño de las secciones por un tema de usario - evaluar uso de colores rojos y verdes
# 3. me parece que hay un tema en el insert a la tabla principal proque los metodos de create e insert son !=
# 4. ...
from tabulate import tabulate
import pandas as pd
import numpy as np
import re
import os

# esto tambien aparece en orquestador_check_query
# hay que ver la forma de que quede como parametro en un mismo lugar
metodos_dic = ({'tipo': {'create':{'prelacion':['check_create_table_as',
                                                'check_query_anidada',
                                                'check_select_all',
                                                'check_prefijo_tabla',
                                                'check_prefijo_campo',
                                                 'check_prefijo_join',
                                                'check_formato_airflow',
                                                'check_errorcode']} #'check_db_bcimkt'
                         ,'insert':{'prelacion':['check_query_anidada',
                                                 'check_par_create_insert',
                                                 'check_select_all',
                                                 'check_groupby',
                                                 'check_fechas',
                                                 'check_db_bcimkt',
                                                 'check_or',
                                                 'check_prefijo_tabla',
                                                 'check_prefijo_campo',
                                                 'check_prefijo_join',
                                                 'check_currentdate',
                                                 'check_fechas_duras',
                                                 'check_formato_airflow',
                                                 'check_likes',
                                                 'check_alias',
                                                 'check_query_comentada',
                                                 'check_errorcode']}
                         ,'drop': {'prelacion':['check_db_bcimkt',
                                                'check_prefijo_tabla',
                                                'check_formato_airflow']}
                         ,'collect':{'prelacion':['check_errorcode',
                                                  'check_db_bcimkt',
                                                  'check_prefijo_tabla',
                                                  'check_prefijo_campo',
                                                  'check_formato_airflow']}
                         }
                })  # Almacenamos info

recorrido_full = {'tipo':['check_collect',
                  'check_quit',
                  'check_inicio_fin',
                  'check_encabezado',
                  'check_tbls_encabezado',
                  'check_drop_finales']}


def cambio_etiquetas_log(df):
    df = df.replace(np.nan, '-', regex=True)
    df = df.replace(False, 'Ok', regex=True)
    df = df.replace(True, 'Fix', regex=True)
    #df = df.replace('BUG', 'Bug', regex=True)
    return(df)

def generador_log_bteq(sql_evaluado, path):
    
    # recuperamos nombre de la query y la ruta del directorio
    split_by_directory = path.split("/")
    nombre_archivo = split_by_directory[len(split_by_directory)-1]
    posicion_nombre = path.find(nombre_archivo)
    nombre_directorio = path[0:posicion_nombre]
    
    # generamos el directorio para carpeta de log
    directorio_log =  re.sub(r"BTEQs", "LOGs", nombre_directorio)
    name_log = nombre_archivo[0:len(nombre_archivo)-4] + ".txt"
    
    construccion_txt = ""
    num_queries = len(sql_evaluado)
    
    seccion_queries = sql_evaluado[0]
    seccion_transversal = sql_evaluado[1]

    
    # paso 0: Vemos el resultado de los metodos de codigo
    metodos_code = recorrido_full['tipo']
    df_check_code = pd.DataFrame(metodos_code, columns = ['metodo'])  
    
    col_errores = []
    col_evidencia = []
    for check_transversal in seccion_transversal:
        col_errores = col_errores + [check_transversal['error']]
        
        listToStr = ' ; '.join([str(elem) for elem in check_transversal['evidencia']])
        col_evidencia = col_evidencia + [listToStr]
        
    df_check_code = pd.DataFrame({'metodo' : metodos_code,
                                  'resutado' : col_errores,
                                  'evidencia' : col_evidencia})  
    
    df_check_code = cambio_etiquetas_log(df_check_code)
    
    
    # paso 1: creamos una tabla principal que tiene todos los metodos de chequeo query
    # esta tabla se encuentra vacia inicialmete
    # pendiente ver como escalar para todos los metodos
    df = pd.DataFrame({'query': pd.Series(dtype='int'),
                       'select_all': pd.Series(dtype='str'),
                       'create_table_as': pd.Series(dtype='str')})
    
    met1 = metodos_dic['tipo']['create']['prelacion']
    met2 = metodos_dic['tipo']['insert']['prelacion']
    met3 = metodos_dic['tipo']['drop']['prelacion']
    met4 = metodos_dic['tipo']['collect']['prelacion']
    metodos = met1 + met2 + met3 + met4
    metodos = ['query'] + list(set(metodos)) 
    df_check_query = pd.DataFrame(columns = metodos)  
    
    
    # paso 2: Procedimiento para insertar valores en nuestra tabla principal
    for num_query in range(0,len(seccion_queries)): # recorremos cada una de las quries
        #print(num_query)
        # En esta sección solo me intersa si el chequeo cumple o no
        # pendiente: ver como indexar de forma mas inteligente
        fila = [num_query+1] + seccion_queries[num_query]['error']
        nombre_cols = ['query'] + seccion_queries[num_query]['metodo']
        df = pd.DataFrame([fila], columns = nombre_cols)  
    
        #df_base = df_base.append([df])
        #pd.concat([df_base,df], axis=0, ignore_index=True)
        df_check_query = df_check_query.append(df, ignore_index = True)
    
    df_check_query = cambio_etiquetas_log(df_check_query)
    
    # convertimos el dataframe en una tabla string amigable para imprimir en el txt
    df_str_code = tabulate(df_check_code, headers='keys', tablefmt='psql')
    df_str_queries = tabulate(df_check_query, headers='keys', tablefmt='psql')
    
    # paso 3: construimos algunas secciones de encabezado para dar orden al log y almacenamos en texto
    # -- el resumen principal con el cumplimiento de los chequeos para cada query
    construccion_txt  = ""
    separador = "################################################################################################"
    titulo_1 = "## ------------------------------- RESUMEN GENERAL QUERIES ----------------------------------- ##"
    titulo_2 = "## ------------------------------- RESUMEN GENERAL CODIGO ----------------------------------- ##"
    separador_a = "## ------------------------------------------------------------------------------------------ ##"
    separador_1 = "## ----------------------------------- QUERY EN CHEQUEO ------------------------------------- ##"
    separador_2 = "## -------------------------------- RESULTADOS DE METODOS ----------------------------------- ##"
    
    # -- Resumen con chequeos de codigo
    construccion_txt = construccion_txt + separador + "\n"
    construccion_txt = construccion_txt + titulo_2 + "\n"
    construccion_txt = construccion_txt + separador + "\n"
    construccion_txt = construccion_txt + df_str_code # imprimimos la tabla resumen con los metodos
    construccion_txt = construccion_txt + '\n\n'
    
    # -- Resumen con chequeos de queries
    construccion_txt = construccion_txt + separador + "\n"
    construccion_txt = construccion_txt + titulo_1 + "\n"
    construccion_txt = construccion_txt + separador + "\n"
    construccion_txt = construccion_txt + df_str_queries # imprimimos la tabla resumen con los metodos
    construccion_txt = construccion_txt + '\n\n'
    
    # -- Resumen con detalle de chequeo de queries 
    # paso 4: Procedimiento para generar el log para cada una de las queries
    num_query = 1
    for query in seccion_queries:
        query_codigo = query['objeto_query']['query'].strip()
        construccion_txt = construccion_txt + separador + "\n"
        construccion_txt = construccion_txt + separador_1 + "\n"
        construccion_txt = construccion_txt + separador + "\n"
        construccion_txt = construccion_txt + "----- QUERY_" + str(num_query) + "\n"
        construccion_txt = construccion_txt + query_codigo + "\n\n"
        construccion_txt = construccion_txt + separador_2 + "\n"
        # Procedimiento para recorrer cada metodo imprimir si cumplio o no y la evidencia si existe
        # -- en caso de que exista el error
        for chequeo in range(0,len(query['error'])):
            metodo = query['metodo'][chequeo]
            chequeo_error = query['error'][chequeo]
            if chequeo_error == True:
                estado = 'ERROR'
                evidencia = query['evidencia'][chequeo]
            else:
                estado = 'OK'
                evidencia = '-'
            string_log = metodo + "   " + str(estado) + "   " + str(evidencia)
            construccion_txt = construccion_txt + string_log + "\n"
        construccion_txt = construccion_txt + separador_a + "\n"
        construccion_txt = construccion_txt + "\n\n"
        num_query += 1
    
    # Paso 5: Creamos directorio de log si no existe    
    if not os.path.exists(directorio_log):
        os.makedirs(directorio_log)
    
    # Paso 5: Procedimiento para almacenar el log
    textfile = open(directorio_log + name_log, 'w')
    textfile.write(construccion_txt)
    textfile.close()
    
    return("log generado: " + name_log)

